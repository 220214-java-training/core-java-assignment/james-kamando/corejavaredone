package com.revature.eval.java.core;

public class ReversedString {
    public static void main(String[] args) {
        // Get Main String into Array
        String strInput = "doG TON si natas";
// Print original Name
        System.out.println ("The Original String " + strInput);
        String strMyName[] = strInput.split ("");
//Get index of the last character
        int LastCharacter = strMyName.length - 1;
// Get curent reversed character
        String strCurrentReversed = "";
        // Get Fianl Reversed String
        String strfinalString = "";
        // loop through the characters
        for (int i = 0; i < strMyName.length; i++) {
            // Store the last character here
            strCurrentReversed = strMyName[LastCharacter];
            // Concatenate (add last character to exiting one )e
            strfinalString = strfinalString + strCurrentReversed;
            //Print temp results
//System.out.println("The current char " +strCurrentReversed);
//System.out.println("The Temp reversed is " +strfinalString);
            //decrease last character by one
            LastCharacter--;
        }
        System.out.println ("The Final reversed is " + strfinalString);
    }
}
